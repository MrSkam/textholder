﻿namespace TextHolder.Views
{
    partial class DownloadFromDatabaseDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DownloadFromDatabaseDialog));
            this.filesList = new System.Windows.Forms.ListBox();
            this.contentRichTextBox = new System.Windows.Forms.RichTextBox();
            this.arrowPictureBox = new System.Windows.Forms.PictureBox();
            this.downloadButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.arrowPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // filesList
            // 
            this.filesList.FormattingEnabled = true;
            this.filesList.ItemHeight = 25;
            this.filesList.Location = new System.Drawing.Point(12, 12);
            this.filesList.Name = "filesList";
            this.filesList.Size = new System.Drawing.Size(314, 429);
            this.filesList.TabIndex = 1;
            this.filesList.SelectedIndexChanged += new System.EventHandler(this.FilesList_SelectedIndexChanged);
            // 
            // contentRichTextBox
            // 
            this.contentRichTextBox.Location = new System.Drawing.Point(373, 12);
            this.contentRichTextBox.Name = "contentRichTextBox";
            this.contentRichTextBox.ReadOnly = true;
            this.contentRichTextBox.Size = new System.Drawing.Size(459, 429);
            this.contentRichTextBox.TabIndex = 5;
            this.contentRichTextBox.Text = "";
            // 
            // arrowPictureBox
            // 
            this.arrowPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("arrowPictureBox.Image")));
            this.arrowPictureBox.Location = new System.Drawing.Point(308, 166);
            this.arrowPictureBox.Name = "arrowPictureBox";
            this.arrowPictureBox.Size = new System.Drawing.Size(86, 103);
            this.arrowPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.arrowPictureBox.TabIndex = 6;
            this.arrowPictureBox.TabStop = false;
            // 
            // downloadButton
            // 
            this.downloadButton.Location = new System.Drawing.Point(12, 461);
            this.downloadButton.Name = "downloadButton";
            this.downloadButton.Size = new System.Drawing.Size(820, 55);
            this.downloadButton.TabIndex = 7;
            this.downloadButton.Text = "Download";
            this.downloadButton.UseVisualStyleBackColor = true;
            this.downloadButton.Click += new System.EventHandler(this.DownloadButton_Click);
            // 
            // DownloadFromDatabaseDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 531);
            this.Controls.Add(this.downloadButton);
            this.Controls.Add(this.contentRichTextBox);
            this.Controls.Add(this.filesList);
            this.Controls.Add(this.arrowPictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DownloadFromDatabaseDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Download";
            this.Shown += new System.EventHandler(this.DownloadFromDatabaseDialog_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.arrowPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox filesList;
        private System.Windows.Forms.RichTextBox contentRichTextBox;
        private System.Windows.Forms.PictureBox arrowPictureBox;
        private System.Windows.Forms.Button downloadButton;
    }
}