﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Windows.Forms;
using TextHolder.Controllers;

namespace TextHolder.Views
{
    public partial class SaveToDatabaseDialog : Form
    {
        public string DefaultExt;
        public string FileName;
             
        public SaveToDatabaseDialog()
        {
            InitializeComponent();
            filesList.DataSource = DataBaseController.instance.files.Local.ToBindingList();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;

            var tempName = fileNameTextBox.Text;

            FileName = tempName;
        }

        private void SaveToDatabaseDialog_Shown(object sender, EventArgs e)
        {
            fileNameTextBox.Text = FileName;
        }
    }
}
