﻿using System.Data.Entity;
using System.Linq;
using TextHolder.Models;

namespace TextHolder.Controllers
{
    public class DataBaseController : DbContext
    {
        #region Singleton

        public static DataBaseController instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DataBaseController();
                return _instance;
            }
            set { }
        }

        #endregion

        private static DataBaseController _instance;

        public DbSet<Record> files { get; set; }

        private DataBaseController() : base("DefaultConnection")
        {
            Database.SetInitializer<DataBaseController>(null);

            files.Load();
        }
        
        /// <summary>
        /// Adding file record to database
        /// </summary>
        /// <param name="name">File name</param>
        /// <param name="file">Serealized file object</param>
        public void AddFile(string name, byte[] file)
        {
            files.Add(new Record(name, file));
            SaveChanges();
        }

        /// <summary>
        /// Get record from database by index
        /// </summary>
        public Record GetRecordByIndex(int index)
        {
            return files.ToArray()[index];
        }
    }
}