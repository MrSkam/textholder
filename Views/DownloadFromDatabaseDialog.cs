﻿using System;
using System.Data.Entity;
using System.Windows.Forms;
using TextHolder.Controllers;
using TextHolder.Models;

namespace TextHolder.Views
{
    public partial class DownloadFromDatabaseDialog : Form
    {
        public int SelectedIndex;

        public DownloadFromDatabaseDialog()
        {
            InitializeComponent();
            filesList.DataSource = DataBaseController.instance.files.Local.ToBindingList();
        }

        private void FilesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (filesList.SelectedIndex != -1)
            {
                var record = (Record) filesList.SelectedItem;
                var textFile = FileController.instance.GetFileFromRecord(record);

                contentRichTextBox.Text = textFile.content;
            }
        }

        private void DownloadButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            if (filesList.SelectedIndex != -1)
            {
                DialogResult = DialogResult.OK;
                SelectedIndex = filesList.SelectedIndex;
            }
            else
                DialogResult = DialogResult.Cancel;
        }

        private void DownloadFromDatabaseDialog_Shown(object sender, EventArgs e)
        {
            filesList.ClearSelected();
            contentRichTextBox.Text = "";
        }
    }
}
