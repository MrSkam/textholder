﻿using System;
using System.IO;
using ProtoBuf;
namespace TextHolder.Models
{
    [ProtoContract]
    public class TextFile
    {
        public string name
        {
            get
            {
                //if (string.IsNullOrEmpty(path))
                //    return "New File";
                //else
                    return Path.GetFileNameWithoutExtension(path);
            }
            set { }
        }

        public string fileExtension
        {
            get
            {
                //if (string.IsNullOrEmpty(path))
                //    return ".txt";
                //else
                    return Path.GetExtension(path);
            }
            set { }
        }

        [ProtoMember(1)]
        public string path { get; set; }

        [ProtoMember(2)]
        public string content { get; set; }

        public TextFile(string path, string content = "")
        {
            this.path = path;
            this.content = content;
        }

        public TextFile()
        {
            path = "New File.txt";
            content = "";
        }

        public string GetFullName()
        {
            return name + fileExtension;
        }
    }
}