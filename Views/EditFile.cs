﻿using System;
using System.Windows.Forms;
using TextHolder.Controllers;
using System.Diagnostics;
using System.Drawing;
using TextHolder.Models;
using FastColoredTextBoxNS;
using TextHolder.Helpers;

namespace TextHolder.Views
{
    public partial class EditFile : Form
    {
        private SaveToDatabaseDialog _saveToDatabaseDialog;
        private DownloadFromDatabaseDialog _downloadFromDatabaseDialog;

        private TextStyle brownStyle;
        private TextStyle blueStyle;
        private TextStyle redStyle;

        public EditFile()
        {
            InitializeComponent();

            Text = FileController.instance.GetCurrentFile().GetFullName();

            openFileDialog.DefaultExt = "txt";
            openFileDialog.Filter = "All files (*.*)|*.*|Json files (*.json)|*.json|XML files (*.xml)|*.xml|Text files (*.txt)|*.txt";

            saveFileDialog.DefaultExt = "txt";
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|Json files (*.json)|*.json|XML files (*.xml)|*.xml";

            _saveToDatabaseDialog = new SaveToDatabaseDialog();
            _saveToDatabaseDialog.DefaultExt = "txt";

            _downloadFromDatabaseDialog = new DownloadFromDatabaseDialog();

            FileController.instance.SubscribeOnCurrentFileChanged(RefreshFile);

            brownStyle = new TextStyle(Brushes.Brown, null, FontStyle.Regular);
            blueStyle = new TextStyle(Brushes.Blue, null, FontStyle.Regular);
            redStyle = new TextStyle(Brushes.Red, null, FontStyle.Regular);
        }

        #region StripMenuEvents

        private void NewFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (TrySaveFile() != DialogResult.Cancel)
            {
                FileController.instance.NewFile();
                contentRichTextBox.ClearUndo();
            }
        }

        private void OpenFromFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (TrySaveFile() != DialogResult.Cancel)
            {
                openFileDialog.FileName = "";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    FileController.instance.LoadFile(openFileDialog.FileName);
                    contentRichTextBox.ClearUndo();
                }
            }
        }

        private void OpenFromDBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_downloadFromDatabaseDialog.ShowDialog() == DialogResult.OK)
            {
                FileController.instance.DownloadFileFromDatabaseAsync(_downloadFromDatabaseDialog.SelectedIndex);
                contentRichTextBox.ClearUndo();
            }
        }

        private void SaveToFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFile();
            RefreshFormattingSettings(FileController.instance.GetCurrentFile().fileExtension);

        }

        private void SaveFileAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileAs();
            RefreshFormattingSettings(FileController.instance.GetCurrentFile().fileExtension);
        }

        private void SaveFileToDBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _saveToDatabaseDialog.FileName = FileController.instance.GetCurrentFile().GetFullName();

            if (_saveToDatabaseDialog.ShowDialog() == DialogResult.OK)
            {
                FileController.instance.SaveToDataBase(contentRichTextBox.Text, _saveToDatabaseDialog.FileName);
            }
        }

        private void OpenRepositoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(@"https://bitbucket.org/MrSkam/textholder/");
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormatDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FileController.instance.GetCurrentFile().fileExtension.ToLower() == ".json")
                contentRichTextBox.Text = TextFormatter.FormatJson(contentRichTextBox.Text);
            else if (FileController.instance.GetCurrentFile().fileExtension.ToLower() == ".xml")
            {
                contentRichTextBox.Text = TextFormatter.FormatXML(contentRichTextBox.Text);
            }
        }

        #endregion

        private void EditFile_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!FileController.instance.CompareContent(contentRichTextBox.Text))
            {
                if (TrySaveFile() == DialogResult.Cancel)
                    e.Cancel = true;
            }
        }

        private void ContentFastColoredTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (FileController.instance.GetCurrentFile().fileExtension.ToLower() == ".json")
            {
                e.ChangedRange.ClearStyle(brownStyle);
                e.ChangedRange.ClearStyle(blueStyle);

                e.ChangedRange.SetStyle(brownStyle, "\"(.*?)\"|\"(.*?)$");
                e.ChangedRange.SetStyle(blueStyle, "(true)|(false)|[0-9]");
            }
            else if (FileController.instance.GetCurrentFile().fileExtension.ToLower() == ".xml")
            {
                e.ChangedRange.ClearStyle(brownStyle);
                e.ChangedRange.ClearStyle(blueStyle);
                e.ChangedRange.ClearStyle(redStyle);
                
                e.ChangedRange.SetStyle(blueStyle, @"<|>|""(.*?)""");
                e.ChangedRange.SetStyle(redStyle, @"(\S+)=");
                e.ChangedRange.SetStyle(brownStyle, @"<\/(.*?)>|<(.*?)>|<(.*?) |<(.*?)$");
            }

            ShowFileChanged();
        }

        private void RefreshFile(TextFile file)
        {
            RefreshFormattingSettings(FileController.instance.GetCurrentFile().fileExtension);

            Invoke((MethodInvoker) delegate
            {
                contentRichTextBox.Text = file.content;
                ShowFileChanged();
            });
        }

        private void ShowFileChanged()
        {
            if (FileController.instance.CompareContent(contentRichTextBox.Text))
                Text = FileController.instance.GetCurrentFile().GetFullName();
            else
                Text = FileController.instance.GetCurrentFile().GetFullName() + " (*)";
        }

        private void RefreshFormattingSettings(string fileExtension)
        {
            Invoke((MethodInvoker) delegate
            {
                if (fileExtension.ToLower() == ".xml" || fileExtension.ToLower() == ".json")
                {
                    formatDocumentToolStripMenuItem.Text = String.Format("Format {0} Document",
                        fileExtension.ToLower() == ".xml" ? "XML" : "Json");
                    formatDocumentToolStripMenuItem.Enabled = true;
                }
                else
                {
                    formatDocumentToolStripMenuItem.Text = "Fromat Documnet";
                    formatDocumentToolStripMenuItem.Enabled = false;
                }
            });
        }

        #region Save functions


        private void SaveFileAs()
        {
            saveFileDialog.FileName = FileController.instance.GetCurrentFile().GetFullName();

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileController.instance.SaveFileAs(contentRichTextBox.Text, saveFileDialog.FileName);
            }
        }

        private void SaveFile()
        {
            if (!FileController.instance.SaveFile(contentRichTextBox.Text))
            {
                SaveFileAs();
            }
        }

        private DialogResult TrySaveFile()
        {
            if (!FileController.instance.CompareContent(contentRichTextBox.Text))
            {
                var dialogResult = MessageBox.Show("Save current file?", "Text Holder", MessageBoxButtons.YesNoCancel);
                switch (dialogResult)
                {
                    case DialogResult.Yes:
                        SaveFile();
                        return DialogResult.Yes;
                    case DialogResult.Cancel:
                        return DialogResult.Cancel;
                }
            }

            return DialogResult.No;
        }

        #endregion

        #region My sloution for code highlighting json

        //void HightlightJson()
        //{
        //    contentRichTextBox.TextChanged -= ContentRichTextBox_TextChanged;
        //    ChangeColorByPattern("\"(.*?)\"|\"(.*?)$", Color.Brown);
        //    ChangeColorByPattern("(true)|(fales)|[0-9]", Color.Blue);
        //    contentRichTextBox.TextChanged += ContentRichTextBox_TextChanged;
        //}

        //public void ChangeColorByPattern(string pattern,Color color)
        //{
        //    var stringRegex = new Regex(pattern);

        //    for (var matchIndex=0;matchIndex<stringRegex.Matches(contentRichTextBox.Text).Count;matchIndex++)
        //    {
        //        var match = stringRegex.Matches(contentRichTextBox.Text)[matchIndex];
        //        contentRichTextBox.Select(match.Index, match.Value.Length);
        //        contentRichTextBox.SelectionColor = color;
        //    }

        //    contentRichTextBox.Select(contentRichTextBox.TextLength,0);
        //    contentRichTextBox.SelectionColor = Color.Black;
        //    contentRichTextBox.SelectionFont = new Font(contentRichTextBox.Font, FontStyle.Regular);
        //}

        #endregion

    }
}