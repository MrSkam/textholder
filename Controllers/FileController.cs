﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProtoBuf;
using TextHolder.Models;

namespace TextHolder.Controllers
{
    public class FileController
    {
        #region Singleton

        public static FileController instance
        {
            get
            {
                if (_instance == null)
                    _instance = new FileController();
                return _instance;
            }
            set { }
        }

        private static FileController _instance;

        #endregion

        private TextFile _currentFile;

        private Action<TextFile> _onCurrentFileChanged;

        private FileController()
        {
            NewFile();
        }
        /// <summary>
        /// Creating new file
        /// </summary>
        public void NewFile()
        {
            _currentFile = new TextFile();
            OnCurrentFileChanged(_currentFile);
        }

        /// <summary>
        /// Upload file to reader
        /// </summary>
        /// <param name="path">Path to file</param>
        /// <returns>Return uploaded file</returns>
        public void LoadFile(string path)
        {
            var content = File.ReadAllText(path);
            UpdateCurrentFile(content,path);
        }

        /// <summary>
        /// Saving file on disc by old path if file exist
        /// </summary>
        /// <returns>Return true if file saved</returns>
        public bool SaveFile(string content)
        {
            if (!File.Exists(_currentFile.path))
                return false;

            UpdateCurrentFile(content);
            SaveFileAs(_currentFile.content, _currentFile.path);
            return true;
        }

        /// <summary>
        /// Saving file on disc
        /// </summary>
        /// <param name="content">File contetn</param>
        /// <param name="path">Saving path</param>
        public void SaveFileAs(string content, string path)
        {
            UpdateCurrentFile(content, path);
            File.WriteAllText(_currentFile.path, _currentFile.content);
        }

        /// <summary>
        /// Saving file to database
        /// </summary>
        /// <param name="content">File content</param>
        /// <param name="name">File name</param>
        public async void SaveToDataBase(string content, string name)
        {
            UpdateCurrentFile(content);

            var formatter = new BinaryFormatter();

            using (var stream = new MemoryStream())
            {
                Serializer.Serialize(stream, _currentFile);

                await Task.Run(() => DataBaseController.instance.AddFile(name, stream.ToArray()));
            }
        }

        /// <summary>
        /// Downloading file from record
        /// </summary>
        /// <param name="record">Recors from database</param>
        public async void DownloadFileFromDatabaseAsync(int selectedIndex)
        {
            await Task.Run(() =>
            {
                var record = DataBaseController.instance.GetRecordByIndex(selectedIndex);
                ConvertRecordToTextFile(record);
            });
        }

        /// <summary>
        /// Conver database record to textfile
        /// </summary>
        /// <param name="record"></param>
        public void ConvertRecordToTextFile(Record record)
        {
                var file = GetFileFromRecord(record);
                UpdateCurrentFile(file.content, file.path);
        }

        /// <summary>
        /// Deserializing file from record
        /// </summary>
        /// <param name="record"></param>
        public TextFile GetFileFromRecord(Record record)
        {
            var foramatter = new BinaryFormatter();
            using (var stream = new MemoryStream(record.File))
            {
                var tempFile = Serializer.Deserialize<TextFile>(stream);
                return tempFile;
            }
        }

        /// <summary>
        /// Opened file getter
        /// </summary>
        public TextFile GetCurrentFile()
        {
            return _currentFile;
        }

        /// <summary>
        /// Compare content in opened file
        /// </summary>
        public bool CompareContent(string newContent)
        {
            return _currentFile.content == newContent;
        }

        #region Update file functions  

        /// <summary>
        /// Updating content in file
        /// </summary>
        private void UpdateCurrentFile(string content)
        {
            _currentFile.content = content;

            OnCurrentFileChanged(_currentFile);
        }

        /// <summary>
        /// Updating content and path in file
        /// </summary>
        private void UpdateCurrentFile(string content, string path)
        {
            _currentFile.content = content;
            _currentFile.path = path;

            OnCurrentFileChanged(_currentFile);
        }

        #endregion


        /// <summary>
        /// Subscribing on file change event
        /// </summary>
        /// <param name="subscribe"></param>
        public void SubscribeOnCurrentFileChanged(Action<TextFile> subscribe)
        {
            _onCurrentFileChanged += subscribe;
        }


        /// <summary>
        /// Unsubscribing from file change event
        /// </summary>
        public void UnsubscribeOnCurrentFileChanged(Action<TextFile> subscribe)
        {
            _onCurrentFileChanged -= subscribe;
        }


        /// <summary>
        /// On file change event
        /// </summary>
        /// <param name="file"></param>
        private void OnCurrentFileChanged(TextFile file)
        {
            if (_onCurrentFileChanged != null)
                _onCurrentFileChanged(file);
        }
    }
}