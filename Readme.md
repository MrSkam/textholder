# Text Holder
![](https://bitbucket.org/MrSkam/textholder/raw/5b451b23db9dd737d7fc2470955ac3fdf979e60d/Images/Screenshots/MainFrom.jpg)

##Point 1: 
![](https://bitbucket.org/MrSkam/textholder/raw/5b451b23db9dd737d7fc2470955ac3fdf979e60d/Images/Screenshots/1AppConfig.jpg)
##Point 2:
![](https://bitbucket.org/MrSkam/textholder/raw/5b451b23db9dd737d7fc2470955ac3fdf979e60d/Images/Screenshots/2%20Donwload%20from%20db.jpg)
##Point 3:
![](https://bitbucket.org/MrSkam/textholder/raw/5b451b23db9dd737d7fc2470955ac3fdf979e60d/Images/Screenshots/3%20Upload%20to%20db.jpg)
##Point 4:
![](https://bitbucket.org/MrSkam/textholder/raw/5b451b23db9dd737d7fc2470955ac3fdf979e60d/Images/Screenshots/4%20Upload%20to%20db.jpg)
##Point 5:
![](https://bitbucket.org/MrSkam/textholder/raw/5b451b23db9dd737d7fc2470955ac3fdf979e60d/Images/Screenshots/5%20Donwload%20from%20db.jpg)

##Point 6:
###Downloading:

```
public async void DownloadFileFromDatabaseAsync(int selectedIndex)
{
    await Task.Run(() =>
    {
        var record = DataBaseController.instance.GetRecordByIndex(selectedIndex);
        ConvertRecordToTextFile(record);
    });
}
```
###Uploading

```
public async void SaveToDataBase(string content, string name)
{
    UpdateCurrentFile(content);

    var formatter = new BinaryFormatter();

    using (var stream = new MemoryStream())
    {
        Serializer.Serialize(stream, _currentFile);

        await Task.Run(() => DataBaseController.instance.AddFile(name, stream.ToArray()));
    }
}
```

For updating RichTextBox I used event onCurrentFileChanged(because if I try update line by line, async file update performed after updating UI). If file was change all subscribers on event get new TextFile object 

```
public void SubscribeOnCurrentFileChanged(Action<TextFile> subscribe)
{
	_onCurrentFileChanged += subscribe;
}

public void UnsubscribeOnCurrentFileChanged(Action<TextFile> subscribe)
{
	_onCurrentFileChanged -= subscribe;
}

private void OnCurrentFileChanged(TextFile file)
{
	if (_onCurrentFileChanged != null)
		_onCurrentFileChanged(file);
}
```

##Point 7:
For serialization data I used Protobuf because it is very fast and economical.
```
using (var stream = new MemoryStream())
{
    Serializer.Serialize(stream, _currentFile);
}
            
using (var stream = new MemoryStream(record.File))
{
    var tempFile = Serializer.Deserialize<TextFile>(stream);
    return tempFile;
}
```         

###Benchmark
![](https://bitbucket.org/MrSkam/textholder/raw/f939112d302235050ac479f849c309580aa85a2e/Images/Screenshots/Benchmark.jpg)

##Point 8:
For formatting json files I used simple function for parsing json and for xml I used standart formatting function. For highlighting I used Regex and FastColoredTextBox, because standard RichTextBox so slow.
![](https://bitbucket.org/MrSkam/textholder/raw/09c5812a07a2d75f1887dfb4b850e6c211b7b55e/Images/Screenshots/XmlFormatting.jpg)
![](https://bitbucket.org/MrSkam/textholder/raw/09c5812a07a2d75f1887dfb4b850e6c211b7b55e/Images/Screenshots/JsonFormatting.jpg)
